# Ngteamwaveinterview


## Setup the credentials for authentication
For an application this size implementing an authentication flow would take an enormous amount 
of time and effort and testing to get it right. Hence the decision was made to emebed the credentials
into the `environment.ts` and `environment.prod.ts` file, under the USERNAME and PASSWORD keys. If you'd like
to see me implement a proper authentication flow, I have a repository privated in my github that has all of it.

Your `environment.ts` and `environment.prod.ts` should look like this

```angular2html
  export const environment = {
      production: false | true,
      API_PREFIX: '[protocol]://[host]:[port]/api/v1/',
      USERNAME: '<username>',
      PASSWORD: '<password>',
  };
```
