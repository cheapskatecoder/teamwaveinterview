import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';
import {HttpClient} from '@angular/common/http';
import {environment} from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private http: HttpClient) {
    this.http.post(environment.API_PREFIX + 'token',
      {username: environment.USERNAME, password: environment.PASSWORD}).subscribe((res) => {
      // @ts-ignore
      localStorage.setItem('access', res.access);
      // @ts-ignore
      localStorage.setItem('refresh', res.refresh);
    });
  }
}
