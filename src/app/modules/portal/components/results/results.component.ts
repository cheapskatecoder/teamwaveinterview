import {Component, Input, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ColumnMode} from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {
  @Input() stackOverFlowResults: any;

  rows = [];
  ColumnMode = ColumnMode;

  constructor(private http: HttpClient) {
    // console.log(this.stackOverFlowResults);
    this.http.get('https://swimlane.github.io/ngx-datatable/assets/data/company.json').subscribe((res) => {
      console.log('asdf');
    });
  }

  ngOnInit(): void {
    this.stackOverFlowResults.forEach(item => {
      this.rows.push({
        link: item.link,
        score: item.score,
        title: item.title,
        view_count: item.view_count,
        is_answered: item.is_answered,
        question_id: item.question_id,
        answer_count: item.answer_count,
      });
    });
    console.log('final rows', this.rows);
  }

}
