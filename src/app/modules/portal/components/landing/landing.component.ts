import { Component, OnInit } from '@angular/core';
import { LandingService } from '../../services/landing.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {

  results;
  allDates = true;
  allNumbers = false;
  allStrings = false;

  searchForm = new FormGroup({
    to_date: new FormControl(''),
    from_date: new FormControl(''),
    sort_by: new FormControl(''),
    min: new FormControl(''),
    tag: new FormControl(''),
    max: new FormControl(''),
    page: new FormControl(),
    pageSize: new FormControl(),
    order: new FormControl(),
  });

  bioSection = new FormGroup({
    firstName: new FormControl(''),
  });
  errorString;

  constructor(private landingService: LandingService) {
    this.landingService.testWithCachedData().subscribe((res) => {
      this.results = res;
    });
  }

  changeFormFields(event): void {
    if (event.target.value === 'activity' || event.target.value === 'creation') {
      this.allNumbers = false;
      this.allStrings = false;
      this.allDates = true;
    } else if (event.target.value === 'votes') {
      this.allStrings = false;
      this.allNumbers = true;
      this.allDates = false;
    } else if (event.target.value === 'hot' || event.target.value === 'week' || event.target.value === 'month') {
      this.allStrings = true;
      this.allNumbers = false;
      this.allDates = false;
    }
  }

  getSearchResults(submittedSearchFormValue): void {
    const payload = {};

    if (this.allDates && !this.allNumbers && !this.allStrings) {
      payload['min'] = new Date(submittedSearchFormValue.min).getTime() / 1000;
      payload['max'] = new Date(submittedSearchFormValue.max).getTime() / 1000;
    } else {
      payload['min'] = submittedSearchFormValue.min;
      payload['max'] = submittedSearchFormValue.max;
    }

    payload['todate'] = new Date(submittedSearchFormValue.to_date).getTime() / 1000;
    payload['fromdate'] =  new Date(submittedSearchFormValue.from_date).getTime() / 1000;

    payload['page'] = submittedSearchFormValue.page;
    payload['page-size'] = submittedSearchFormValue.pageSize;
    payload['sort'] = submittedSearchFormValue.sort_by;
    payload['tag'] = submittedSearchFormValue.tag;
    payload['order'] = submittedSearchFormValue.order;

    this.landingService.getSearchResults(payload).subscribe((e) => {
      console.log(e);
    });
  }

  ngOnInit(): void {
  }

}
