import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LandingService {

  constructor(private http: HttpClient) {
  }

  getSearchResults(payload): Observable<any>{
    return this.http.post(environment.API_PREFIX + 'search', payload);
  }

  testWithCachedData(): Observable<any> {
    return this.http.get(environment.API_PREFIX + 'query/1');
  }
}
