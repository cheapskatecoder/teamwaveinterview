import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpClientModule} from '@angular/common/http';


import { PortalRoutingModule } from './portal-routing.module';
import { LandingComponent } from './components/landing/landing.component';
import { ResultsComponent } from './components/results/results.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';

@NgModule({
  declarations: [LandingComponent, ResultsComponent],
  imports: [
    CommonModule,
    PortalRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgxDatatableModule
  ]
})
export class PortalModule { }
